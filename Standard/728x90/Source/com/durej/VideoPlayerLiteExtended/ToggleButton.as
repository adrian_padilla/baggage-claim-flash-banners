/**
 * @author Slavomir Durej
 */

class com.durej.VideoPlayerLiteExtended.ToggleButton extends MovieClip 
{
	function ToggleButton() 
	{
		stop();
	}

	public function gotoStatus(playing:Boolean):Void
	{
		if (playing) this.gotoAndStop(2);
		else this.gotoAndStop(1);
	}
	
	
	
	
}