/**
 * @author Slavomir Durej
 */
import mx.utils.Delegate;

class com.durej.VideoPlayerLiteExtended.SeekBar {
	
	private var loaded_mc		:MovieClip;
	private var playing_mc		:MovieClip;
	private var dragBtn_mc		:MovieClip;
	private var barWidth		:Number;
	public var onScrub			:Function;
	
	function SeekBar(loaded_mc:MovieClip,playing_mc:MovieClip,dragBtn_mc:MovieClip) 
	{
		this.loaded_mc 		= loaded_mc;
		this.playing_mc 	= playing_mc;		this.dragBtn_mc		= dragBtn_mc;
		barWidth 			= playing_mc._width;
		
		init();
	}
	
	private function init():Void
	{
		dragBtn_mc.onPress 			= Delegate.create(this,startScrubbing);
		dragBtn_mc.onRelease 		= Delegate.create(this,stopScrubbing);
		dragBtn_mc.onReleaseOutside	= Delegate.create(this,stopScrubbing);
		
		playing_mc._alpha = dragBtn_mc._alpha = 0;
	}
	
	public function onLoadProgress(pctLoaded:Number):Void
	{
		loaded_mc._xscale = pctLoaded;
	}
	
	public function onPlayProgress(pctPlayed:Number):Void
	{
		if (pctPlayed > 0) playing_mc._alpha = dragBtn_mc._alpha = 100;

		playing_mc._xscale = pctPlayed;
		dragBtn_mc._x = playing_mc._x + playing_mc._width - Math.floor(dragBtn_mc._width/100*pctPlayed);
	}
	
	private function startScrubbing():Void
	{
		loaded_mc.onEnterFrame = Delegate.create(this,scrub);
	}
	
	private function scrub():Void
	{
		var xPos = loaded_mc._x + Math.floor(loaded_mc._xmouse);
		var xPos:Number = Math.max(loaded_mc._x,Math.min(xPos,loaded_mc._x+loaded_mc._width-dragBtn_mc._width)); //limit xpos
		
		var perc:Number = Math.floor(((xPos-loaded_mc._x)/barWidth)*100);
		onScrub(perc);
		
		dragBtn_mc._x = xPos;	
	}
	
	private function stopScrubbing():Void
	{
		delete loaded_mc.onEnterFrame;
	}

}