﻿/**
 * @author Slavomir Durej
 * ©2008 durej.com
 */
import mx.transitions.OnEnterFrameBeacon;
import mx.utils.Delegate;

class com.durej.VideoPlayerLiteExtended.FLVLoaderLite {

	//compulsory vars passed via constructor
	private var mediaUrl			:String;			//flv file that is to be played
	private var timeline			:MovieClip;			//timeline is a movie clip that is wrapping video object
	
	//optional switches 
	public var loop					:Boolean = false;	//enables video looping
	public var autostart			:Boolean = true;	//determines whether video is paused at the beginning
	public var soundOnStart			:Boolean = true;	//determines whether audio is muted at the begining
	
	//configurable public buffering values	
	public var minBufferVal			:Number = 2;		//default number of seconds of playback available before playing can start
	public var maxBufferVal			:Number = 5;		//increased number of seconds for buffering after minBufferVal is insufficient for continuous playback

	//loader properties
	public var playing				:Boolean = false;	//determines whether playback is in progress
	public var nDuration			:Number;			//duration of the flv. Only available after and if metadata has been received
	public var soundOn				:Boolean = true;	//mute on / off status of the player
	
	//loader events	
	public var onLoadProgress		:Function;
	public var onPlayProgress		:Function;
	public var onPlaybackFinished	:Function;
	public var onError				:Function;
	public var onMediaStart			:Function;
	public var onMediaStop			:Function;
	public var updateButtonStatus	:Function;	
	
	private var ncVideo				:NetConnection;
	private var nsVideo				:NetStream;
	private var videoObj_vid		:Video;	
	private var trackingEngine		:Object;
	private var loadCheckingEngine	:Object;

	private var flvAudio			:MovieClip;
	private var movieSound			:Sound;
	private var pausedOnce			:Boolean = false;

				
	function FLVLoaderLite(mediaUrl:String, timeline:MovieClip)
	{
		
		this.timeline 			= timeline;
		this.mediaUrl 			= mediaUrl;
		this.videoObj_vid 		= timeline.videoObj;
		
		init();
	}
	
	private function init():Void
	{
		if (soundOnStart == undefined) soundOnStart = false;
		OnEnterFrameBeacon.init();
		
		minBufferVal = 2;
		maxBufferVal = 15;
		
		ncVideo = new NetConnection();
		ncVideo.connect(null);
		
		nsVideo = new NetStream(ncVideo);
		nsVideo.play(mediaUrl);
		nsVideo.setBufferTime(minBufferVal);
		
		nsVideo.onMetaData = Delegate.create(this,onNetStreamMetadata);
		nsVideo.onStatus = Delegate.create(this,onNetStreamStatus);
		nDuration = 0;
		
		//audio setup
		if (flvAudio == undefined) flvAudio = timeline.createEmptyMovieClip("flvAudio", 8741);
		flvAudio.attachAudio(nsVideo);
		
		movieSound = new Sound(flvAudio);
	}

	
	private var statusCode			:String;
	
	private function onNetStreamStatus(oStatus:Object):Void
	{
		
		statusCode = oStatus.code;
		
		//trace ("onNetStreamStatus statusCode"+statusCode+"   oStatus.level : "+oStatus.level);
		
		
		//------------------------------------------------------------------------ //when playhed reached the end rewind to beginning
		if (oStatus.level == "status" && statusCode == "NetStream.Play.Stop") 
        {
       		if (loop)
       		{
       			rewToStart();
       		}
       		 else 
       		{
	       		playing = false;
	       		updateButtonStatus();
	        	onMediaStop();
       		} 
       	}
       	
       	if (oStatus.level == "status" && statusCode == "NetStream.Play.Start") 
       	{
			if (!soundOnStart || !soundOn)
			{
				 mute();
			}
			if (autostart)
			{
				playing = true;
				updateButtonStatus();
	       		onMediaStart();
			}
       	}
       	
       	
       	//-------------------------------------------------------------------------------- ERROR MANAGEMENT
        if (oStatus.level == "error")
        {
        	onError("ERROR : "+statusCode);
        }
        //--------------------------------------------------------------------------------- DYNAMIC BUFFERING
        if (statusCode =="NetStream.Buffer.Full")
        {
        	/*
        	 if (!autostart&& !pausedOnce) 
       		{
       			pauseMedia();
       			movieSound.setVolume(100);
       			pausedOnce = true;
       		}
       		 * 
       		 */
        	nsVideo.setBufferTime(maxBufferVal);
        }
        else if (statusCode =="NetStream.Buffer.Empty")
        {
        	nsVideo.setBufferTime(minBufferVal);
        }
	}
	
	public function playPauseToggle():Void
	{
		if (playing)
		{
			pauseMedia();
		}
		else
		{
			playMedia();
		}
	}
		
	public function mute():Void
	{

		if (soundOn)
		{
			movieSound.setVolume(0);
			soundOn = false;
		}
		else
		{
			movieSound.setVolume(100);
			soundOn = true;
		}
		updateButtonStatus();
	}
	
		
	public function loadVideo():Void
	{
		videoObj_vid.attachVideo(nsVideo);
		startPlayheadTrackingEngine();
		startLoadCheckingEngine();
		
		
		if (!autostart)
		{
			pauseMedia();
		}
		else
		{
			playMedia();
		}
	}		

	public function pauseMedia() : Void 
	{
		nsVideo.pause(true);
		playing = false;
		updateButtonStatus();
	}

	public function playMedia():Void
	{
		nsVideo.pause(false);

		playing = true;
		updateButtonStatus();
	}
	
	
	public function rewToStart():Void
	{
		nsVideo.seek(0);
		pauseMedia();
		playMedia();
	}
	
	public function scrub(perc:Number):Void
	{
		var seekTime:Number =nDuration/100* perc;
		nsVideo.seek(seekTime);		
	}

	private function onNetStreamMetadata(oMetaData:Object):Void
	{
		nDuration = oMetaData.duration;	
      
        var vidWidth:Number 	= oMetaData.width;
        var vidHeight:Number 	= oMetaData.height;
        
        videoObj_vid._width 	= vidWidth;
        videoObj_vid._height 	= vidHeight;
	}

	
	
	function onFLVLoadProgress () : Void 
	{
		var totalBytes:Number  	= nsVideo.bytesTotal;
		var loadedBytes:Number  = nsVideo.bytesLoaded;

		if (totalBytes > 4)
		{
			var pctLoaded:Number = Math.floor(loadedBytes/totalBytes*100);
			onLoadProgress(pctLoaded);
			if (pctLoaded >= 100) stopLoadCheckingEngine();
		}
	}
	
	
	private function startLoadCheckingEngine():Void
	{
		loadCheckingEngine = new Object();
		loadCheckingEngine.onEnterFrame = Delegate.create(this,onFLVLoadProgress);
		MovieClip.addListener(loadCheckingEngine);			
	}
	
	private function stopLoadCheckingEngine():Void
	{
		MovieClip.removeListener(loadCheckingEngine);
	}	
	
	
	private function startPlayheadTrackingEngine():Void
	{
		trackingEngine = new Object();
		trackingEngine.onEnterFrame = Delegate.create(this,onPlayheadProgress);
		MovieClip.addListener(trackingEngine);			
	}
	
	private function stopPlayheadTrackingEngine():Void
	{
		MovieClip.removeListener(trackingEngine);
	}	
	
	
	function onPlayheadProgress () : Void 
	{
		var nPercent:Number = 100 * nsVideo.time / nDuration;
		onPlayProgress(Math.floor(nPercent));
	}
	
	public function destroy():Void
	{
		stopPlayheadTrackingEngine();
		stopLoadCheckingEngine();
		nsVideo.close();
		ncVideo.close();
		videoObj_vid.clear();
	}

}